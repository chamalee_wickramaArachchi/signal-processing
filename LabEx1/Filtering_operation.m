%% Task 1
clear;
clc; close all;
N=10;
L=5;
h_n = rand(1,L);
var_x = 3;
var_w =1;
%% Generation of X[n]

X_n = randn(N,1)*sqrt(var_x);  % generate vector with normally distributed random values with variance of 3 

subplot(5,1,1)
stem([1:N]-1,X_n,'fill','r')
grid on;
title('input signal, X[n]=')
xlabel('time n')
subplot(5,1,2)
stem([1:L]-1,h_n,'fill','r')
grid on;
title('impulse sequence, h(n)=')
xlabel('time n')
ylabel('amplitude')
%% Generation of Y[n]
tic;
Y_n_1 = my_filter(X_n,h_n);
myfilt_t = toc; %time taken by my filter function

tic;
Y_n_2 = filter(h_n,1,X_n); % FIR filter using Matlab built-in function
matlab_t = toc % time taken by matlab filter function

filt_error = (Y_n_1-Y_n_2)';

xCorr_Y = corrcoef(Y_n_1,Y_n_2);

subplot(5,1,3)
stem([1:length(Y_n_1)]-1,Y_n_1,'fill','r')
grid on;
title('filter output (my filter), Y[n]=')
xlabel('time n')
ylabel('amplitude')
subplot(5,1,4)
stem([1:length(Y_n_2)]-1,Y_n_2,'fill','r')
grid on;
title('filter output (built-in), Y[n]=')
xlabel('time n')
ylabel('amplitude')
%% Generation of noise
Z_n = randn(N,1);

%% Output U[n]

U_n = Y_n_1 + Z_n;

subplot(5,1,5);
stem([1:length(U_n)]-1,U_n,'fill','r');
grid on;
title('output signal,U[n]=');
xlabel('time n');
ylabel('amplitude');

%% %PSD of the signal U[n]

%%PSD theoretical calcualtion for the L-Tap filter
H_w =zeros(N,1);
for k =1:1:L
    for n=1:1:N %(i-1)/N is the normalized frequency
        H_w(n,1) = H_w(n,1) + h_n(k)*exp(-1j*(k-1)*2*pi*(n-1)/N);%Calculating the frequency response of the channel with given taps
    end
end
psd_x = var_x; %PSD of x 
psd_w = var_w/2;%PSD of white noise
psdu_th = abs(H_w).^2*psd_x + psd_w;%Output PSD calculation 

figure;
stem(psdu_th);% the theoretical PSD
grid on;
title('PSD of the Output Signal U[n]');
xlabel('Normalized Frequency');
ylabel('Power/Hz');

% Numerical PSD of U_n
U_w_n = my_fourier(U_n);% DFT of the output signal
psdu_num = (1/N).*abs(U_w_n).^2;% PSD of U_n
hold on; 
stem(psdu_num,':diamondr');
legend('Theretical PSD','Numerical PSD'); 